Scilab Matmark Toolbox

Purpose
-------

Matmark provides functions to manage Matrix Market files.
These functions are a Scilab port of the Matlab functions provided 
at :

http://math.nist.gov/MatrixMarket/mmio/matlab/mmiomatlab.html

Features
--------

   * mminfo : read information from a Matrix Market file
   * mmread : reads a Matrix Market file
   * mmwrite : writes a sparse or dense matrix to a Matrix Market file

Dependencies
------------

 * This module depends on the assert module.
 * This module depends on the apifun module.

License
-------

This toolbox is released under the terms of the CeCILL license :
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

Authors
-------
 * 2009 - 2010 - INRIA - Serge Steer
 * 2011 - Benoit Goepfert
 * 2011 - DIGITEO - Michael Baudin
