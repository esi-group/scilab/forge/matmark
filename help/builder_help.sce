// ====================================================================
// Copyright (C) 2009 - INRIA - Serge Steer
// Copyright (C) 2011 - Benoit Goepfert
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

tbx_builder_help_lang("en_US",get_absolute_file_path("builder_help.sce"));
